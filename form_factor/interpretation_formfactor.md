# Interprétation des résultats pour le facteur de forme

## 1. Le facteur de forme interprétation
![Schema1](../annexe/schema1.png "schema1").

## 2. Explication du code
     Le code présent ici permet d'effectuer une analyse de sensibilité sur le paramètre form factor. En entrée, le fichier json de la couche est nécessaire.
     La première étape consiste à ouvrir et lire ce fichier json avec python. La fonction '''changement_couche_sommet''' permet de modifier la géométrie des polygones de la couche et de calculer, une fois la géométrie modifiée, la moyenne du facteur de forme pour l'ensemble des bâtiments.
     
## 3. Analyse des résultats
